GameProgress 設計書

画面遷移
========================================
```uml
@startuml

hide empty description
[*]                        --> メニュー画面
メニュー画面               ->  進捗サマリ画面
進捗サマリ画面             --> メニュー画面
進捗サマリ画面             --> 英雄譚画面
進捗サマリ画面             --> 英雄試練画面
進捗サマリ画面             --> 究極戦争遊戯画面
進捗サマリ画面             ->  進捗設定ウィンドウ
英雄譚画面                 --> 進捗サマリ画面
英雄譚画面                 --> 英雄譚編集ウィンドウ
英雄譚画面                 --> メニュー画面
英雄試練画面               --> 進捗サマリ画面
英雄試練画面               --> 英雄試練編集ウィンドウ
英雄試練画面               --> メニュー画面
究極戦争遊戯画面           --> 進捗サマリ画面
究極戦争遊戯画面           --> 究極戦争遊戯編集ウィンドウ
究極戦争遊戯画面           --> メニュー画面
進捗設定ウィンドウ         ->  進捗サマリ画面
英雄譚編集ウィンドウ       --> 英雄譚画面
英雄試練編集ウィンドウ     --> 英雄試練画面
究極戦争遊戯編集ウィンドウ --> 究極戦争遊戯画面

state 進捗設定ウィンドウ #FF88FF
state 英雄譚編集ウィンドウ #FF88FF
state 英雄試練編集ウィンドウ #FF88FF
state 究極戦争遊戯編集ウィンドウ #FF88FF

@enduml
```

フォルダ構成
========================================
```
GameProgress.exe
└Data
　├
　└Progress
　　├AdventureStory
　　│└P_Adv_0000001.json
　　├HeroTrial
　　│└P_Her_0000001.json
　　├UltimateWarGame
　　│└P_Alt_0000001.json
　　├Foster
　　│└P_Fos_0000001.json
　　└ProgressSetting.json
```

ファイル名（DataSource）
========================================

対象         |Name
-------------|-
冒険譚       |`P_Adv_XXXXXXX`
英雄試練     |`P_Her_XXXXXXX`
究極戦争遊戯 |`P_Alt_XXXXXXX`
育成         |`P_Fos_XXXXXXX`

クラス構成
========================================

メッセージ通知ウィンドウ
----------------------------------------

```uml
@startuml

package "GameProgress.View.Notification" {
  class NotificationView
  class NotificationAction
  class SetResultAction
}

package "GameProgress.ViewModel.Notification" {
  class NotificationViewModel
  class BidingData
}

package "GameProgress.Model.Notification" {
  class NotificationParam
  enum ButtonType
  enum IconType
  enum NotificationResult
}
Window <|-- NotificationView
TriggerAction <|-- NotificationAction
TriggerAction <|-- SetResultAction
NotificationView -- NotificationParam
NotificationAction -- NotificationParam
SetResultAction -- NotificationResult
NotificationView -- NotificationViewModel
NotificationViewModel -- BidingData
NotificationViewModel -- NotificationParam
NotificationViewModel -- NotificationResult
NotificationParam o-- ButtonType
NotificationParam o-- IconType

@enduml
```

メニュー画面
----------------------------------------
```uml
@startuml

package "GameProgress.View.Menu" {
  class MenuView
  class MenuAction
}
package "GameProgress.ViewModel.Menu" {
  class MenuViewModel
}
Page <|-- MenuView
TriggerAction <|-- MenuAction
MenuView -- MenuViewModel

@enduml
```

進捗 サマリ画面
----------------------------------------
```uml
@startuml

package "GameProgress.View.Progress.Summary" {
  class SummaryView
  class SummaryAction
}
package "GameProgress.ViewModel.Progress.Summary" {
  class SummaryViewModel
  class BindingData
  class AdventureStorySummary
  class HeroTrialSummary
  class UltimateWarGameSummary
  class IntermediateData
}
Page <|-- SummaryView
TriggerAction <|-- SummaryAction
SummaryView -- SummaryViewModel
SummaryViewModel -- BindingData
BindingData -- AdventureStorySummary
BindingData -- HeroTrialSummary
BindingData -- UltimateWarGameSummary
SummaryViewModel -- IntermediateData

@enduml
```

進捗 サマリ設定画面
----------------------------------------
```uml
@startuml

package "GameProgress.View.Progress.SettingPregoress" {
  class SettingPregoressView
  class SettingPregoressAction
}
package "GameProgress.ViewModel.Progress.SettingProgress" {
  class SettingProgressViewModel
  class BindingData
  class IntermediateData
}
Window <|-- SettingPregoressView
TriggerAction <|-- SettingPregoressAction
SettingPregoressView -- SettingProgressViewModel
SettingProgressViewModel -- BindingData
SettingProgressViewModel -- IntermediateData

@enduml
```

進捗 冒険譚画面
----------------------------------------
```uml
@startuml

package "GameProgress.View.Progress.AdventureStory" {
  class AdventureStoryView
  class AdventureStoryAction
}
package "GameProgress.ViewModel.Progress.AdventureStory" {
  class AdventureStoryViewModel
  class IntermediateData
  class BindingData
  class BindDailyHistory
  class BindGuildReward
}
Page <|-- AdventureStoryView
TriggerAction <|-- AdventureStoryAction
AdventureStoryView -- AdventureStoryViewModel
AdventureStoryViewModel -- IntermediateData
IntermediateData -- BindingData
BindingData -- BindDailyHistory
BindingData -- BindGuildReward

@enduml
```

進捗 冒険譚編集画面
----------------------------------------
```uml
@startuml

package "GameProgress.View.Progress.EditAdventureStory" {
  class EditAdventureStoryView
  class EditAdventureStoryAction
}
package "GameProgress.ViewModel.Progress.AdventureStory" {
  class EditAdventureStoryViewModel
  class BindingData
  class IntermediateData
  class BindExchangeItem
}
Window <|-- EditAdventureStoryView
TriggerAction <|-- EditAdventureStoryAction
EditAdventureStoryView -- EditAdventureStoryViewModel
EditAdventureStoryViewModel -- BindingData
EditAdventureStoryViewModel -- IntermediateData
BindingData -- BindExchangeItem

@enduml
```

進捗 英雄試練画面
----------------------------------------
```uml
@startuml

package "GameProgress.View.Progress.HeroTrial" {
  class HeroTrialView
  class HeroTrialAction
}
package "GameProgress.ViewModel.Progress.HeroTrial" {
  class HeroTrialViewModel
  class BindingData
}
Page <|-- HeroTrialView
TriggerAction <|-- HeroTrialAction
HeroTrialView -- HeroTrialViewModel
HeroTrialViewModel -- BindingData

@enduml
```

進捗 究極戦争遊戯画面
----------------------------------------
```uml
@startuml

package "GameProgress.View.Progress.UltimateWarGame" {
  class UltimateWarGameView
  class UltimateWarGameAction
}
package "GameProgress.ViewModel.Progress.UltimateWarGame" {
  class UltimateWarGameViewModel
  class BindingData
}
Page <|-- UltimateWarGameView
TriggerAction <|-- UltimateWarGameAction
UltimateWarGameView -- UltimateWarGameViewModel
UltimateWarGameViewModel -- BindingData

@enduml
```

GameProgress.View
----------------------------------------
```uml
@startuml

package "GameProgress.View.Main" {
  class MainView
}

@enduml
```

```uml
@startuml

package "GameProgress.View.Progress" {
  package "EditAdventureStory" {
    class EditAdventureStoryView
    class EditAdventureStoryAction
  }
}

@enduml
```

```uml
@startuml

package "GameProgress.View.Progress" {
  package "EditHeroTrial" {
    class EditHeroTrialAction
    class EditHeroTrialView
  }
}

@enduml
```

```uml
@startuml

package "GameProgress.View.Progress" {
  package "" {
    class HeroTrialAction
    class HeroTrialView
  }
  package "EditHeroTrial" {
    class EditHeroTrialAction
    class EditHeroTrialView
  }
}

@enduml
```

GameProgress.ViewModel
----------------------------------------

```uml
@startuml

package "GameProgress.ViewModel.Progress.EditAdventureStory" {
  class EditAdventureStoryViewModel
  class BindingData
}

@enduml
```

GameProgress.Model
----------------------------------------

```uml
@startuml

package "GameProgress.Model.Infrastructure" {
 class Messenger
 class Message
 class MessengerEventArgs
 class MessengerTrigger
}

@enduml
```

```uml
@startuml

package "GameProgress.Model.TextFile" {
  interface ITextSerializerFactory
  class JsonSerializerFactory
  class XmlSerializerFactory
  class FileReadResult
  class FileWriteResult
  enum FileAccessStatus
  interface ITextSerializer
  class JsonSerializer
  class XmlSerializer
}
ITextSerializer o-- ITextSerializerFactory
ITextSerializer o-- FileReadResult
ITextSerializer o-- FileWriteResult
ITextSerializerFactory <|-- JsonSerializerFactory
ITextSerializerFactory <|-- XmlSerializerFactory
FileReadResult o-- FileAccessStatus
FileWriteResult o-- FileAccessStatus
ITextSerializer <|-- JsonSerializer
ITextSerializer <|-- XmlSerializer

@enduml
```

クラス詳細
========================================

メッセージ通知ウィンドウ
----------------------------------------

### View
```uml
@startuml

class NotificationView {
  + {static} NotificationResult : Result
  + void NotificationView(NotificationParam parameter)
}
class NotificationAction {
  # void Invoke(object parameter)
}
class SetResultAction {
  # void Invoke(object parameter)
}

@enduml
```

### ViewModel
```uml
@startuml

class NotificationViewModel {
  + void Initialize(Notification parameter)
  + {property} Messenger<NotificationResult> : SetResultMessenger
  .. Positive button ..
  + {property} DelegateCommand : SelectPositiveCommand
  - void SelectPositive()
  .. Negative button ..
  + {property} DelegateCommand : SelectNegativeCommand
  - void SelectNegative()
  .. Cancel button ..
  + {property} DelegateCommand : SelectCancelCommand
  - void SelectCancel()
}
class BindingData {
  + {property} string : Message
  + {property} IconType : Icon
  + {property} string : PositiveContent
  + {property} string : NegativeContent
  + {property} bool : NegativeButtonVisibility
  + {property} bool : CancelButtonVisibility
  + void : Update(Notification parameter)
}

@enduml
```

### Model
```uml
@startuml

class NotificationParam {
  + {property} string : Message
  + {property} ButtonType : Button
  + {property} IconType : Icon
}
enum ButtonType {
  Ok
  OkCancel
  YesNo
  YesNoCancel
}
enum IconType {
  Information
  Warning
  Error
}
enum NotificationResult {
  Ok
  No
  Cancel
}

@enduml
```

メニュー画面
----------------------------------------

### View
```uml
@startuml

class MenuView {
  + {static} {property} MenuView : MenuViewObject
  + void MenuView()
}
note bottom: Singleton（MainViewからnewするのでコンストラクタは公開する）
class MenuAction {
  # void Invoke(object parameter)
}

@enduml
```

### ViewModel
```uml
@startuml

class MenuViewModel {
  .. Progress button ..
  + {property} Messenger : ShowProgressPageMessenger
  + {property} DelegateCommand : ShowProgressPageCommand
  - void ShowProgressPage()
}

@enduml
```

進捗 サマリ画面
----------------------------------------
### View
```uml
@startuml

class SummaryView {
  - {static} {property} SummaryView : SummaryViewObject
  - void SummaryView()
}
note bottom: Singleton
class SummaryAction {
  # void Invoke(object parameter)
}

@enduml
```

### ViewModel
```uml
@startuml

class SummaryViewModel {
  .. Page loaded Event ..
  + {property} DelegateCommand : PageLoadedCommand
  - void UpdateView()
  - void UpdateViewOneShot()
  .. Notify Message ..
  + {property} Messenger : NotifyMessenger
  .. Menu button ..
  + {property} Messenger : ShowMenuPageMessenger
  + {property} DelegateCommand : ShowMenuPageCommand
  - void ShowMenuPage()
  .. Setting button ..
  + {property} Messenger : ShowSettingProgressPageMessenger
  + {property} DelegateCommand : ShowSettingProgressPageCommand
  - void ShowSettingProgressPage()
  .. 「Adventure Story」 Select ..
  + {property} Messenger : ShowAdventureStoryPageMessenger
  + {property} DelegateCommand : SelectAdventureStoryPageCommand
  - void ShowAdventureStory()
  .. 「Hero Trial」 Select ..
  + {property} Messenger : ShowHeroTrialPageMessenger
  + {property} DelegateCommand : SelectHeroTrialPageCommand
  - void ShowHeroTrial()
  .. 「Ultimate War Game」 Select ..
  + {property} Messenger : ShowUltimateWarGamePageMessenger
  + {property} DelegateCommand : SelectUltimateWarGamePageCommand
  - void ShowUltimateWarGame()
}
class BindingData {
  .. Adventure Story ..
  + {property} ObservableCollection<AdventureStorySummary> AdventureStorySummarys
  + {property} AdventureStorySummary SelectedAdventureStory
  .. Hero Trial ..
  + {property} ObservableCollection<HeroTrialSummary> HeroTrialSummarys
  + {property} HeroTrialSummary SelectedHeroTrial
  .. Ultimate War Game ..
  + {property} ObservableCollection<UltimateWarGameSummary> UltimateWarGameSummarys
  + {property} UltimateWarGameSummary SelectedUltimateWarGame
  .. Update ..
  + void Update(List<AdventureStroyData> data)
  + void Update(List<HeroTrialData> data)
  + void Update(List<UltimateWarGameData> data)
}
class AdventureStorySummary {
  + {property} string : EventId
  + {property} string : Title
  + {property} string : Date
  + {property} string : UncommonActual
  + {property} Decimal : UncommonTargetDailyRate
  + {property} string : UncommonTargetDaily
  + {property} Decimal : UncommonTargetRate
  + {property} string : UncommonTarget
  + {property} string : CommonActual
  + {property} Decimal : CommonTargetDailyRate
  + {property} string : CommonTargetDaily
  + {property} Decimal : CommonTargetRate
  + {property} string : CommonTarget
  + {property} string : RareActual
  + {property} Decimal : RareTargetDailyRate
  + {property} string : RareTargetDaily
  + {property} Decimal : RareTargetRate
  + {property} string : RareTarget
  + {property} string : GachaActual
  + {property} Decimal : GachaTargetDailyRate
  + {property} string : GachaTargetDaily
  + {property} Decimal : GachaTargetRate
  + {property} string : GachaTarget
}
class HeroTrialSummary {
}
note bottom: 画面を決めないと...
class UltimateWarGameSummary {
}
note bottom: 画面を決めないと...
SummaryViewModel -- BindingData
BindingData -- AdventureStorySummary
BindingData -- HeroTrialSummary
BindingData -- UltimateWarGameSummary

@enduml
```

進捗 サマリ設定画面
----------------------------------------
### View
```uml
@startuml

class SettingPregoressView {
  + SettingPregoressView()
}
class SettingPregoressAction {
  # void Invoke(object parameter)
}

@enduml
```

### ViewModel
```uml
@startuml

class SettingProgressViewModel {
  - IntermediateData : inData
  + BindingData : BindData

  + {property} DelegateCommand : WindowRenderedCommand
  - void : Initialize()
  - void : UpdateAdventureStory()

  .. Notify Message ..
  + {property} Messenger : ShowMenuPageMessenger

  .. Add(AdventureStory) button ..
  + {property} Messenger : ShowEditAdventureStoryDialogMessenger
  + {property} DelegateCommand : AddAdventureStoryCommand
  + {property} DelegateCommand : DeleteAdventureStoryCommand
  + {property} DelegateCommand : EditAdventureStoryCommand

  .. Select change button ..
  + {property} MessengerParameter<ProgressEditUserSelect> : SetResultMessenger
  + {property} DelegateCommand : ConfirmChangeCommand
  + {property} DelegateCommand : DisposeChangeCommand
}
class BindingData {
  + ProgressSettings : ToBasic()
}
class IntermediateData {
  - ProgressSettings : OrgData

  - List<AdventureStroyData> : CurrentAdventureStories
  - List<AdventureStroyData> : PendingAddAdventureStories
  - List<String> : PendingDeleteAdventureStories

  + bool IsChange(ProgressSettings currentSettings)
  + void CommitChange(ProgressSettings currentSettings)
}
note bottom: CommitChangeの戻り値は考えないと

SettingProgressViewModel -- BindingData
SettingProgressViewModel -- IntermediateData

@enduml
```

進捗 冒険譚画面
----------------------------------------
### View

### ViewModel
```uml
@startuml

class AdventureStoryViewModel {
  + Action<AdventureStoryData> : RegisterCallBack()
  - void : SetLoadingData(AdventureStoryData quest)
  + {property} MessengerParameter<NotificationParam> : NotifyMessenger
  .. Page loaded ..
  + {property} DelegateCommand : PageLoadedCommand
  .. 「Menu」button  ..
  + {property} Messenger : ShowMenuPageMessenger
  + {property} DelegateCommand : ShowMenuPageCommand
  - void : ShowMenuPage()
  .. 「Summary」button  ..
  + {property} Messenger : ShowProgressSummaryPageMessenger
  + {property} DelegateCommand : ShowProgressSummaryPageCommand
  - void : ShowProgressSummaryPage()
  .. 「Setting」button  ..
  + {property} MessengerCallback<AdventureStoryData, ProgressEditResult<AdventureStoryData>> : ShowEditAdventureStoryPageMessenger
  + {property} DelegateCommand : ShowEditAdventureStoryPageCommand
  - void : ShowEditAdventureStoryPage()
  .. Guild reward  ..
  + {property} DelegateCommand : AddGuildRewardCommand
  - void : AddGuildReward()
  + {property} DelegateCommand : DeleteGuildRewardCommand
  - void : DeleteGuildReward()
  - bool : CanDeleteGuildReward()
  + {property} DelegateCommand : CopyGuildRewardCommand
  - void : CopyGuildReward()
  - bool : CanCopyGuildReward()
  + {property} DelegateCommand : UpGuildRewardCommand
  - void : UpGuildReward()
  - bool : CanUpGuildReward()
  + {property} DelegateCommand : DownGuildRewardCommand
  - void : DownGuildReward()
  - bool : CanDownGuildReward()
  .. 「Save」button  ..
  + {property} DelegateCommand : ConfirmCommand
  - void : Confirm()
  ..  ..
  - IntermediateData : inData
  + {property} BindingData : BindData
}
class IntermediateData {
  - BindingData : bindData
  - AdventureStoryData : originalQuest
  + {property} AdventureStoryData : LatestQuest
  + {property} string : LatestEventId

  + IntermediateData(BindingData data, AdventureStoryData quest)
  + void : InitialUpdate()
  + void : Update(AdventureStoryData quest)
  + bool : MergeBind()
  + bool : IsModify()
  + void : AddGuildReward()
  + void : DeleteGuildReward()
  + bool : CanDeleteGuildReward()
  + void : CopyGuildReward()
  + bool : CanCopyGuildReward()
  + void : UpGuildReward()
  + bool : CanUpGuildReward()
  + void : DownGuildReward()
  + bool : CanDownGuildReward()
}
class BindingData {
  + {property} string : EventTitle
  + {property} ObservableCollection<BindDailyHistory> : DailyHistories
  + {property} ObservableCollection<BindGuildReward> : BindGuildRewards
  + {property} BindGuildReward : SelectGuildReward
  + {property} int : SelectGuildRewardIndex
  - string : imagePath
  + {property} BitmapImage : Image

  + void : Update(AdventureStoryData quest)
  + AdventureStoryData? : ToBasic(AdventureStoryData quest)
  + bool : CheckValue()
  + void : AddNewGuildAward()
  + void : DeleteGuildAward()
  + bool : CanDeleteGuildAward()
  + void : CopyGuildAward()
  + bool : CanCopyGuildAward()
  + void : UpGuildAward()
  + bool : CanUpGuildAward()
  + void : DownGuildAward()
  + bool : CanDownGuildAward()
  - void : UpdateDailyHistory(AdventureStoryData quest)
  - void : UpdateGuildReward(List<GuildReward> rewards)
  - bool : ShowImage()
}
class BindDailyHistory {
  + BindDailyHistory()
  + AdventureStoryDailyHistory : ToBasic()
  + bool : CheckValue()
}
class BindGuildReward {
  + {property} string : Target
  + {property} string : Condition
  + {property} string : ItemName
  + {property} string : ItemNum
  + BindGuildReward()
  + BindGuildReward(GuildReward reward)
  + BindGuildReward : Clone()
  + GuildReward : ToBasic()
  + bool : CheckValue()
}

AdventureStoryViewModel -- IntermediateData
IntermediateData -- BindingData
BindingData -- BindDailyHistory
BindingData -- BindGuildReward

@enduml
```

進捗 冒険譚編集画面
----------------------------------------
### View
```uml
@startuml

class EditAdventureStoryView {
  + EditAdventureStoryView()
}
class EditAdventureStoryAction {
  # void Invoke(object parameter)
}
class SetResultAction {
  # void Invoke(object parameter)
}

@enduml
```

### ViewModel
```uml
@startuml

class EditAdventureStoryViewModel {
  + void : Initialize()
  - IntermediateData : inData
  + BindingData : BindData

  .. SettingTerm button ..
  + {property} DelegateCommand : DecreaseSettingTermCommand
  + {property} DelegateCommand : IncreaseSettingTermCommand
  .. Image select button ..
  + {property} MessengerCallback<string, string> : ShowFileSelectDialogMessenger
  + {property} DelegateCommand : ShowFileSelectDialogCommand
  - void : ShowFileSelectDialog()
  .. GachaFarm button ..
  + {property} DelegateCommand : DecreaseGachaFarmCommand
  + {property} DelegateCommand : IncreaseGachaFarmCommand
  .. ExchagenItem button ..
  + {property} DelegateCommand : CopyExchangeItemCommand
  + {property} DelegateCommand : AddExchangeItemCommand
  + {property} DelegateCommand : DeleteExchangeItemCommand
  + {property} DelegateCommand : UpExchangeCommand
  + {property} DelegateCommand : DownExchangeCommand
  .. Select change button ..
  + {property} MessengerParameter<ProgressEditResult<AdventureStoryData>> : SetResultMessenger
  + {property} DelegateCommand : ConfirmChangeCommand
  + {property} DelegateCommand : DisposeChangeCommand
}
class BindingData {
  + {property} string : EventTitle
  + {property} DateTime : EventStartDay
  + {property} DateTime : EventEndDay
  + {property} int : EventTerm
  + {property} int : UserSettingTerm
  + {property} string : ImagePath
  + {property} int : MaxGachaFarmNum
  + {property} ObservableCollection<BindExchangeItem> : ExchangeItems
  + {property} BindExchangeItem: SelectExchangeItem
  + {property} int: SelectExchangeItemIndex
  + {property} int : UncommonItemNum
  + {property} int : CommonItemNum
  + {property} int : RareItemNum

  + void : Update(AdventureStoryData data)
  + AdventureStoryData : ToBasic()
  + void : DecreaseUserSettingTerm()
  + void : IncreaseUserSettingTerm()
  + void : DecreaseGachaFarm()
  + void : IncreaseGachaFarm()
  + void : UpSelectedItem()
  + void : DownSelectedItem()
  + void : CalcEventItemNum()
  - void : CalcTermDiff()
}
class IntermediateData {
  - {property} AdventureStoryData : OrgData
  + IntermediateData(AdventureStoryData data)
  + AdventureStoryData : Merge(Func<AdventureStoryData> getData)
}
class BindExchangeItem {
  + {property} string : ItemName
  + {property} bool : UncommonChecked
  + {property} bool : CommonChecked
  + {property} bool : RareChecked
  + {property} int : Num
  + {property} int : Times

  + BindExchangeItem()
  + BindExchangeItem(ExchangeItem data)
  + ExchangeItem : ToBasic()
}
EditAdventureStoryViewModel -- BindingData
EditAdventureStoryViewModel -- IntermediateData
BindingData -- BindExchangeItem

@enduml
```

進捗 英雄試練画面
----------------------------------------
### View

### ViewModel

進捗 究極戦争遊戯画面
----------------------------------------
### View

### ViewModel

GameProgress.View
----------------------------------------
### 進捗設定ウィンドウ
```uml
@startuml

class SettingPregoressView {
  + void SettingPregoressView()
}
class SettingPregoressAction {
  # void Invoke(object parameter)
}

Window <|-- SettingPregoressView
TriggerAction <|-- SettingPregoressAction

@enduml
```

### 冒険譚 進捗画面
```uml
@startuml

class AdventureStoryView {
  - {static} AdventureStoryView : AdventureStoryViewObject
  - void AdventureStoryView()
  + {static} AdventureStoryView GetInstance()
}
note bottom: Singleton
class AdventureStoryAction {
  # void Invoke(object parameter)
}

Page <|-- AdventureStoryView
TriggerAction <|-- AdventureStoryAction

@enduml
```

### 冒険譚 編集ウィンドウ
```uml
@startuml

class EditAdventureStoryView {
  + void EditAdventureStoryView(AdventureStoryData parameter)
}
class EditAdventureStoryAction {
  # void Invoke(object parameter)
}

Window <|-- EditAdventureStoryView
TriggerAction <|-- EditAdventureStoryAction

@enduml
```

GameProgress.Model
----------------------------------------
```uml
@startuml

package "GameProgress.Model.DataSource {
  class DataReadResult<T> {
    + DataAccessStatus : Status
    + T : Data
  }
  class DataWriteResult<T> {
    + DataAccessStatus : Status
    + T : Data
  }
  enum DataAccessStatus {
    Default
    Ok
    OtherApplicationHold
    FileNotFound
    DirectoryNotFound
    Unexpected
  }
}
package "GameProgress.Model.DataSource.Progress {
  interface IProgressDataAccesser<T> {
    + DataReadResult<T> : ReadData(string : eventId)
    + List<DataReadResult<T>> : ReadData()
    + DataWriteResult<T> : WriteData(string eventId, T data)
  }
}

IProgressDataAccesser o-- DataReadResult
IProgressDataAccesser o-- DataWriteResult
DataReadResult o-- DataAccessStatus
DataWriteResult o-- DataAccessStatus

@enduml
```

### データソース 進捗

```uml
@startuml

package "GameProgress.Model.DataSource.Progress" {
  class ProgressDataAccesser {
    {static} DataReadResult<ProgressSettings> : ReadSettings()
    {static} DataWriteResult<ProgressSettings> : WriteSettings(ProgressSettings data)

    {static} DataReadResult<AdventureStroyData> : ReadAdventureStroyData(string eventId)
    {static} DataWriteResult<AdventureStroyData> : WriteAdventureStroyData(string eventId, AdventureStroyData data)
    {static} List<DataReadResult<AdventureStroyData>> : ReadAllAdventureStroyData()
  }
}

@enduml
```

```uml
@startuml

package "GameProgress.Model.DataSource.Progress" {
  class ProgressSettings {
    + ActiveProgressSettings : ActiveProgressSettings
  }
  class ActiveProgressSettings {
    + List<string> : AdventureStoryActiveList
    + List<string> : HeroTrialActiveList
    + List<string> : UltimateWarGameActiveList
    + List<string> : FosterActiveList
  }
}
ProgressSettings o-- ActiveProgressSettings

@enduml
```

### データソース 進捗 冒険譚

```uml
@startuml

package "GameProgress.Model.DataSource.Progress.AdventureStory" {
  class AdventureStroyData {
    + string : EventId
    + string : EventTitle
    + DateTime : EventStartDay
    + DateTime : EventEndDay
    + int : UserSettingTerm
    + string : ImagePath
    + List<ExchangeItem> : ExchangeItems
    + int : MaxGachaFarmNum
    + List<GuildReward> : GuildRewards
    + List<AdventrueStoryDailyHistory> : Histories
  }
  class ExchangeItem {
    + string : ItemName
    + EventItemRank : Rank
    + int : Num
    + int : Times
  }
  enum EventItemRank {
    Uncommon
    Common
    Rare
  }
  class GuildReward {
    + string : RequrementTarget
    + string : RequrementCondition
    + string : RewardItemName
    + int : RewardItemNum
  }
  class AdventrueStoryDailyHistory {
    + int : UncommonItemNum
    + int : CommonItemNum
    + int : RareItemNum
    + int : ToFarmGacha
  }
}

IProgressDataAccesser <|-- AdventureStroyDataAccesser
AdventureStroyDataAccesser o-- AdventureStroyData
AdventureStroyData o-- AdventrueStoryDailyHistory
AdventureStroyData o-- ExchangeItem
ExchangeItem o-- EventItemRank
AdventureStroyData o-- GuildReward

@enduml
```

### データソース 進捗 英雄試練

```uml
@startuml

package "GameProgress.Model.DataSource.Progress.HeroTrial" {
  class HeroTrialData {
    + string : EventId
    + string : EventTitle
    + DateTime : EventStartDay
    + DateTime : EventEndDay
    + int : UserSettingTerm
    + string : ImagePath
    + List<ExchangeItem> : ExchangeItems
    + List<HeroTrialDailyHistory> : Histories
  }
  class ExchangeItem {
    + string : ItemName
    + int : Num
    + int : Times
  }
  class HeroTrialDailyHistory {
    + string : ItemNum
  }
}
HeroTrialData o-- ExchangeItem
HeroTrialData o-- HeroTrialDailyHistory

@enduml
```

### データソース 進捗 究極戦争遊戯

```uml
@startuml

package "GameProgress.Model.DataSource.Progress.UltimateWarGame" {
  class UltimateWarGameData {
    + string : EventId
    + string : EventTitle
    + DateTime : EventStartDay
    + DateTime : EventEndDay
    + int : UserSettingTerm
    + Score : Score
    + List<UltimateWarGameDailyHistory> : Histories
  }
  class Score {
    + int : StartScore
    + int : TargetScore
  }
  class UltimateWarGameDailyHistory {
    + int : Score
  }
}
UltimateWarGameData o-- Score
UltimateWarGameData o-- UltimateWarGameDailyHistory

@enduml
```

### データソース 進捗 育成

```uml
@startuml

package "GameProgress.Model.DataSource.Progress.Foster" {
  class FosterData {
    + string : EventId
    + string : EventTitle
    + DateTime : EventStartDay
    + DateTime : EventEndDay
    + int : UserSettingTerm
    + Score : Score
    + List<FosterDailyHistory> : Histories
  }
  class Level {
    + int : StartLevel
    + int : TargetLevel
  }
  class FosterDailyHistory {
    + int : Level
  }
}
FosterData o-- Level
FosterData o-- FosterDailyHistory

@enduml
```

シーケンス
========================================

進捗 サマリ設定
----------------------------------------
```uml
@startuml

ユーザ -> SettingProgressView : OKボタン押下
SettingProgressView -> SettingProgressViewModel : ConfirmChangeCommand()
SettingProgressViewModel -> BindingData : ToSettings()
SettingProgressViewModel <- BindingData : 結果
SettingProgressViewModel -> IntermediateData : IsChange()
SettingProgressViewModel <- IntermediateData : 結果
alt Modified
  SettingProgressViewModel -> Messenger : Notify()
  SettingProgressViewModel <- Messenger : 結果
  alt Yes
    SettingProgressViewModel -> IntermediateData : CommitChange()
    opt AS追加あり
      IntermediateData -> ProgressDataAccesser : WriteAdventureStroyData()
    end
    opt AS削除あり
      IntermediateData -> ProgressDataAccesser : DeleteAdventureStroyData()
    end
    opt 変更あり
      IntermediateData -> ProgressDataAccesser : WriteSettings()
    end
  else No
    SettingProgressView <- SettingProgressViewModel : 終了
  end
else
  SettingProgressView <- SettingProgressViewModel : 終了
end

@enduml
```


参考
========================================

NavigationWindowクラスを用いた画面遷移
----------------------------------------
別ウィンドウ表示と同様に、messenger方式で実現可能。

別ウィンドウ表示ではXXActionクラスにて、
newした`Window`クラスの`Show()`or`ShowDialog()`を呼び出せばよかったが、
画面遷移ではnewした`Page`クラスを`NavigationWindow.Navigate()`に渡す必要がある。

以下のように、`Application.Current.MainWindow`を`NavigationWindow`として使用し、
`Navigate()`を呼び出す。
（もちろん、ベースのウィンドウは`Window`クラスではなく`NavigationWindow`とする必要がある）
```
    public class XXAction : TriggerAction<DependencyObject>
    {
        protected override void Invoke(object parameter)
        {
            var navigationWindow = (NavigationWindow)Application.Current.MainWindow;
            navigationWindow.Navigate(new XXView(), parameter);
        }
    }

```

### 注意点（メモリリーク）
上記サンプルはメモリリークの問題があるため、使用しないようにすること。

`NavigationWindow.Navigate()`に渡すViewは`Page`クラスとなる。
`Page`クラスは画面上に存在しなくなっても（別ページに遷移した状態）
GCの対象にはならない模様（オブジェクトの参照が残っている？）。
（デストラクタにデバッグメッセージを入れて確認）

よって、上記のサンプルのように画面の遷移要求のたびに`Page`クラスのインスタンスを生成すると、
newした分メモリを占有してしまう（画面遷移する度にメモリの使用量が増加し、強制GCしても解放されない）。

上記のことから`Page`クラスはSingletonパターンで実装すること。

### 注意点（Pageクラスへのデータの受け渡し）
次にPageクラスへ遷移元からデータを渡す際の注意点を記載する。
（現時点でベストな方法であるかは微妙）

別ウィンドウ表示のmessenger方式ではViewクラスのConstractorに引数として渡したいデータを指定し、
`DataContextChanged`イベントに引数のデータを指定していた。
別ウィンドウ表示では、ウィンドウ表示/Closeでインスタンスの生成・破棄が行われ、
ウィンドウ表示時にDataContextChangedを通じて通知が来ていた。

一方`Page`クラスではメモリリークの問題があることから、アプリが生きている間は
ひとつのインスタンスを使いまわすことになる。
よってDataContextChangedは初回生成時の１回しか呼ばれない。
というか、`Page`クラスにはDataContextChangedのイベントがない。
（おそらく`Page`クラスと`Window`クラスはクラスの派生が異なるため、モノが違う）

解決方法の一つとしては、`Loaded()`をフックする。
ちょっと試した限りでは、画面遷移完了後に`Loaded()`が１度呼ばれる。

End
